#include <iostream>
#include <fstream>
#include <string>
#include <vector>




void readFile(std::string filePath, std::vector<std::string>& ret);
void readFile(std::string filePath, char delim, std::vector<std::string>& ret);


int main(int argv, char ** argc){
    if (argv<2){
        std::cout<<"Please specifiy the file to use, recieved arguments: "<<argc <<std::endl;
        
        return 0;
    }else{
        
        std::string fPath = argc[1];

        std::cout <<argv<< " : " << argc[1] << std::endl;
        
        std::cout<< "Path is "<< fPath << std::endl;
        std::vector<std::string> fContents;
        readFile(fPath, fContents);
    }
}



void readFile(std::string filePath, std::vector<std::string>& ret){
    std::cout << "Contents of: "<<filePath<< std::endl;
    std::string line;
    std::ifstream infile(filePath);

    while (std::getline(infile,line)){
            std::cout <<line<<std::endl;
            ret.push_back(line);
    }

    std::cout<<"EOF"<<std::endl;
}



